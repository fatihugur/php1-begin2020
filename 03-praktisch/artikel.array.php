<?php 

$artikels=[
    [   "id" => 1,
        "title" => "My first article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
         autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
         Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
         officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
          autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
          Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
          officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
           autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Bert",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '20-01-2020')
    ],
    [   "id" => 2,
        "title" => "My second article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
         autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
         Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
         officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
          autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
          Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
          officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
           autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Danny",
        "published" => false,
        "date" => DateTime::createFromFormat('d-m-Y', '18-01-2020')
    ],
    [   "id" => 3,
        "title" => "My third article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
         autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
         Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
         officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
          autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
          Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
          officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
           autem totam.</p>",
        "categories" => ["drupal", "php", "drupal2"],
        "author" => "Koen",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '22-01-2020')
    ],
    [   "id" => 4,
        "title" => "My forth article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
         autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
         Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
         officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
          autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
          Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
          officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
           autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Rony",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '25-01-2020')
    ],
    [   "id" => 5,
        "title" => "My fifth article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Stef",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '25-01-2020')
    ],
    [   "id" => 6,
        "title" => "My sixth article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Bert",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '20-01-2020')
    ],
    [   "id" =>7,
        "title" => "My seventh article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Danny",
        "published" => false,
        "date" => DateTime::createFromFormat('d-m-Y', '18-01-2020')
    ],
    [   "id" => 8,
        "title" => "My eight article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["drupal", "php", "drupal2"],
        "author" => "Koen",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '22-01-2020')
    ],
    [   "id" => 9,
        "title" => "My nineth article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Rony",
        "published" => false,
        "date" => DateTime::createFromFormat('d-m-Y', '25-01-2020')
    ],
    [   "id" => 10,
        "title" => "My tenth article",
        "summary" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque ",
        "content" => "<p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p><p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
        Neque voluptas voluptatem nesciunt rerum possimus vero, molestiae incidunt 
        officia quas temporibus nam at eaque laudantium eligendi excepturi ea dolores
        autem totam.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Stef",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '25-01-2020')
    ],
];



