<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My blog</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <div class="container">
        <header class="main-header">
            <h1>My blog</h1>
            <p>Welcome to my personal blog.</p>
        </header>

        
        <main class="main-content">
        <?php if ($artikel['published']== true):?>
            <header>
                <h1 class="page-title">
                    <?php echo $artikel["title"] ?>
                </h1>
                <p>Published by <?php echo $artikel["author"] ?>
                    on <?php echo $artikel['date']->format('d F Y') ?></p>
            </header>

            <ul>
                <?php foreach ($artikel["categories"] as $category) : ?>
                    <li><?php echo $category ?></li>
                <?php endforeach ?>
            </ul>

            <?php echo $artikel['content'] ?>

            <footer>
                <a href="index.php">Back to index</a>
            </footer>
    </div>
        <php else ?>
    <php endif ?>
    </main>
    <footer class="main-footer">
        <p>Copyright CVO De verdieping</p>
    </footer>
    </div>

</body>

</html>