<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My blog</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="container">
    <header class="main-header">
        <h1> My blog</h1>
        <p>Welcome to my personel blog.</p>
    </header> 

    <main class="main-content">
        <h1 class="page-title">Latest Articles</h1>
        <div class="artikels">

            <?php foreach ($artikels as $artikel): ?>
                <?php if ($artikel['published']== true):?>
                <article  class="article-teaser">
                    <header>
                        <h1><a href="artikel.php?id=<?php echo $artikel['id']?>"><?php echo $artikel["title"] ?></a></h1>
                        <p>Published by <?php echo $artikel["author"] ?> 
                        <?php echo $artikel["date"]->format ("d F Y") ?></p>
                    </header> 

                    <p> 
                    <?php echo $artikel['summary'] ?>
                    </p>

                    <footer>
                    <a href="artikel.php?id=<?php echo $artikel["id"] ?>">Lees meer</a>
                    </footer>
                </article>
                <?php endif ?>
            <?php endforeach ?>

        </div>
    </main>

    <footer class="main-footer">
        <p> Copyright CVO de Verdieping</p>
    </footer>
    </div>
</body>
</html>