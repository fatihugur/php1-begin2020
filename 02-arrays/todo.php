<?php 
$todos=[
    [
        "text" => "Huiswerk",
        "completed" => false,
        "assigned_to" => "Muhammed",
        "deadline" => DateTime::createFromFormat('d-m-Y', '14-02-2020'),

    ],
    [
        "text" => "Gras maaien",
        "completed" => false,
        "assigned_to" => "Danny",
        "deadline" => DateTime::createFromFormat('d-m-Y', '18-02-2020'),
    ],
    [
        "text" => "Cursus React schrijven",
        "completed" => false,
        "assigned_to" => "Bert",
        "deadline" => DateTime::createFromFormat('d-m-Y', '14-02-2020'),

    ],
    [
        "text" => "Inkopen doen",
        "completed" => true,
        "assigned_to" => "Steff",
        "deadline" => DateTime::createFromFormat('d-m-Y', '20-02-2020'),

    ],
    

];
include_once "views/todo.view.php";
