<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Todo lijst</title>
    <link rel="stylesheet" href="css/todo-style.css">

</head>  
<body>

<?php foreach ($todos as $todo): ?>
    
<div class="todo <?php echo $todo['completed'] ? "todo-completed": "" ?> ">
    <h1 class="title">Todos</h1>
    <div class="todo">
        <header class="todo-header">
        <h2 class="todo-title"><?php echo $todo["text"] ?></h2>
        <p class="todo-assigned_to">Assigned to <span><?php echo $todo["assigned_to"] ?></span></p>
        <p class="todo-deadline"> To complete before <span><?php echo $todo["deadline"]->format ("d F Y") ?></span></p>
        </header>
        <p class="todo-text">Lorem ipsum lorem ipsum Lorem ipsum lorem ipsumLorem ipsum lorem ipsumLorem ipsum
         lorem ipsumLorem ipsum lorem ipsum </p>
    </div>
</div>
<?php endforeach ?>




</body>
</html>