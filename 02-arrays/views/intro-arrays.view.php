<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>arrays-intro</title>

</head>  
<body>

<h1>Films</h1>

<ul>
    <?php foreach($films as $film): ?>
    <li><?php echo $film ?></li>
    <?php endforeach?>
</ul>

<h1>Vakken</h1>

<ul>
    <?php foreach($vakken as $vaak): ?>
    <li><?php echo $vaak?></li>
    <?php endforeach?>
</ul>


<h1>Informatie over jezelf</h1>
<ul>
<li> Kleur ogen: <?php echo $jezelf["kleur_ogen"] ?></li>
<li> Haarkleur: <?php echo $jezelf["haarkleur"] ?></li>
<li> Lengte: <?php echo $jezelf["lengte"] ?></li>
<li> Favo food: <?php echo implode (', ', $jezelf["favo_food"]) ?></li>
</ul>

<ul>
    <?php  foreach ($jezelf as $eigenschap => $waarde): ?>
    <li> <?php echo $eigenschap ?> = <?php echo $waarde ?>
    <?php endforeach?>
</ul>


<h1>Informatie over jezelf</h1>
<ul>
<li> Kleur ogen: <?php echo $jezelf["kleur_ogen"] ?></li>
<li> Haarkleur: <?php echo $jezelf["haarkleur"] ?></li>
<li> Lengte: <?php echo $jezelf["lengte"] ?></li>
<li> Favo food: <?php echo implode(', ', $jezelf["favo_food"]) ?></li>
</ul>

<h1>Informatie over jezelf</h1>
<ul>
<li> Steff geeft <?php echo $leerkracht["Steff"] ?></li>
<li> Rony geeft <?php echo $leerkracht["Rony"] ?></li>
<li> Bert geeft <?php echo $leerkracht["Bert"] ?></li>
<li> Niels geeft <?php echo $leerkracht["Niels"] ?></li>
<li> Danny geeft <?php echo $leerkracht["Danny"] ?></li>
<li> Bert geeft <?php echo $leerkracht["Bert_1"] ?></li>
<li> Domunique geeft <?php echo $leerkracht["Domunique"] ?></li>
<li> Eva geeft <?php echo $leerkracht["Eva"] ?></li>
<li> X geeft <?php echo $leerkracht["X"] ?></li>
</ul>

<h1>Informatie over leerkracht</h1>
<ul>
<?php  foreach ($leerkracht as $naam => $vaak): ?>
    <li> <?php echo $naam ?> = <?php echo $vaak ?>
    <?php endforeach?>
</ul>

</body>
</html>