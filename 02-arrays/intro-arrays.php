<?php 
//aanmaken arrays

$array1= array("item 1", "item 2","item 3");
$array2= ["item 1", "item 2","item 3"];

$naam = "Fatih";
$array3 = ["item", 12, true, 5.5, $naam];

//array items afdrukken

echo "<pre>";
var_dump($array1);
echo "</pre>";

echo $array1[1];

// array films

$films= [
    "Joker",
    "ToyStory",
    "Jurassic Park",
    "Avengers: Endgame",
    "Tarzan",
];

//array vakken

$vakken= [
    "Startup",
    "Pythone",
    "HTML&CSS",
    "Web-Layout Design",
    "Javascript",
    "PHP",
    "Interface Desigh",
    "Solicitatie training",
    "JQuery"
];


// array film afdrukken

echo "<p> Mijn derde favoriete film is <b>{$films[2]}<b>.</p>";

// 
$films[]= "Spider-man: Into the spider verse";

echo "<pre>";
var_dump($films);
echo "</pre>";


echo "<p> Er zijn ".count($films)." films.</p>";

//afdrukken als list

echo "<ul>";

foreach($films as $film) {
    echo"<li>$film</li>";
}
echo "</ul>";


//afdrukken vakken als list 

echo "<ul>";

foreach($vakken as $vaak) {
    echo"<li>$vaak</li>";
}
echo "</ul>";

include_once "views/intro-arrays.view.php";