<?php

$articles = [
    [
        "id" => 1,
        "title" => "My first article",
        "summary" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque",
        "content" => "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p>",
        "categories" => ["html", "css", "drupal"],
        "author" => "Bert",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '20-01-2020')
    ],
    [
        "id" => 2,
        "title" => "My Second article",
        "summary" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque",
        "content" => "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p>",
        "categories" => ["js", "css", "wordpress"],
        "author" => "Danny",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '22-01-2020')
    ],
    [
        "id" => 3,
        "title" => "My third article",
        "summary" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque",
        "content" => "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p>",
        "categories" => ["html", "css"],
        "author" => "Stef",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '26-01-2020')
    ],
    [
        "id" => 4,
        "title" => "My fourth article",
        "summary" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque",
        "content" => "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p>",
        "categories" => ["html", "jquery", "drupal"],
        "author" => "Bert",
        "published" => false,
        "date" => DateTime::createFromFormat('d-m-Y', '02-02-2020')
    ],
    [
        "id" => 5,
        "title" => "My fifth article",
        "summary" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque",
        "content" => "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis in sit aspernatur quisquam eum beatae, maxime laboriosam molestiae deleniti quos at itaque, sapiente voluptatem fugiat exercitationem, quidem totam sint cupiditate.</p>",
        "categories" => ["drupal", "php"],
        "author" => "Koen",
        "published" => true,
        "date" => DateTime::createFromFormat('d-m-Y', '10-02-2020')
    ],
];