<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My blog</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="container">
    <header class="main-header">
        <h1>My blog</h1>
        <p>Welcome to my personal blog.</p>
    </header>

    <main class="main-content">
        <h1 class="page-title">Latest Articles</h1>
        <div class="articles">
        
        <?php foreach($articles as $article): ?>
            <article class="article-teaser">
                <header>
                    <h1><a href="article.php?id=<?php echo $article['id'] ?>"><?php echo $article["title"] ?></a></h1>
                    <p>Published by <?php echo $article["author"] ?> 
                        on <?php echo $article['date']->format('d F Y') ?></p>
                </header>
                <p>
                    <?php echo $article['summary'] ?>
                </p>
                <footer>
                    <a href="article.php?id=<?php echo $article['id'] ?>">Lees meer</a>
                </footer>
            </article>
        <?php endforeach ?>

        
        </div>
    </main>
    <footer class="main-footer">
        <p>Copyright CVO De verdieping</p>
    </footer>
</div>
    
</body>
</html>