<?php  

/*opervlak van rechthoek*/

$korte_zijde= 400;
$lange_zijde= 50;
$opervlak_rechthoek= $korte_zijde*$lange_zijde ;

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Oefeningen opervlak van rechthoek |HTML&PHP_mengen</title>
    <style>
    .rechthoek{
        display:flex;
        justify-content:center;
        padding: 5rem;
        border: 5px solid dimgray;
        margin: 0;
        background-color:grey;
        align-items:center;
        box-sizing:border-box;
        width: <?php echo $korte_zijde ?>px; 
        height:<?php echo $lange_zijde ?>px;
    }

    </style>

</head>  
<body>

<h1>Opervlakte berekenen</h1>

<form>
<p><b>Vul het formulier in om de oppervlakte te berekenen</b></p>
<p>Wat is de breedte: <input name="korte_zijd" type="text" required> (in cm of meter)</p>
<p>Wat is de hoogte: <input name="lange_zijd" type="text" required>(in cm of meter)</p>
<p> <input type="submit" value="Berekenen!"></p>
</form>


<ul>

<li><p>Breedte: <?php echo $korte_zijde ?></p></li>
<li><p>Hoogte: <?php echo $lange_zijde ?></p></li>
<p>De oppervlakte is:<b> <?php echo $opervlak_rechthoek ?><b>.</p>
</ul>

<div class="rechthoek"> <div>


</body>
</html>