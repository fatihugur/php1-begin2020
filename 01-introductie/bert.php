
<?php  
/*Bert Lempens*/
$naam="Bert Lempens";
$lengte_Bert= 1.85;
$gewicht_Bert= 74;
$bmi_Bert= $gewicht_Bert/($lengte_Bert*$lengte_Bert);

/*Berekenen eigen BMI

$lengte=$_GET ['lengte']?? 1.85;
$gewicht=$_GET ['gewicht']?? 74;
$bmi = $gewicht /($lengte*$lengte);
*/

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Variabelen - Typen - Oefening</title>
    <style>

        body{
            width:1000px;
            margin:auto;
        }
        .bmi-berekenen{
            display:flex;
            justify-content:center;
            padding: 5rem;
            border: 5px solid lightgrey;
            margin: 0;
            background-color:#f2f2f2;
            align-items:center;
            box-sizing:border-box;
        }

        h1{
            font-size:1.3rem;
            margin:20px;
            color:darkblue;
            
        }
        
        form{
            width:600px;
            background-color:lightblue;
            padding-left:30px;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            padding:10px;
            text-align:center;
            margin:auto;
        
        }

        input[type=text] {
            width: 30%;
            padding: 6px 8px;
            margin: 4px 0;
            border: 1px solid #ccc;
            border-radius: 4px;
            margin-left:10px;
            }

        input[type=submit] {
            width:80%;
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 16px 32px;
            text-decoration: none;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 4px;
            margin-left:10px;
            }
        ul{
            background-color:darkblue;
            color:white;
            padding:10px;
            list-style:none;
            padding-left:15px;
            border-radius: 4px;
            }

        .laatste {
            margin:20px;
            font-size:20px;
        }

    </style>
</head>  
<body>

<p>Bert Lempens is <?php ec $lengte_Bert ?> lengte en <?php eco $gewicht_Bert ?> gewicht. Zijn BMI is <?php eco $bmi_Bert ?>.</p>

<div class="bmi-berekenen">

<h1>Bereken je eigen <abbr title="Body mass index">BMI</abbr><br> met het formulier</h1>

<form>
<p><b>Vul het formulier in om je eigen <abbr title="Body mass index">BMI</abbr> te berekenen.</b></p>
<p>Lengte: <input name="lengte" type="text" required>  (in meter)</p>
<p>Gewicht: <input name="gewicht" type="text" required> (in kg)</p>
<p> <input type="submit" value="Berekenen"></p>
</form>

<h1><abbr title="Body mass index">BMI</abbr></h1>
<ul>
<li>Lengte: <?php echo $lengte ?>m </li>
<li>Gewicht: <?php echo $gewicht ?>kg </li>
</ul>

<p class="laatste">Het <abbr title="Body mass index">BMI</abbr> is <b><?php echo round($bmi,2) ?><b>.</p>
</div>
<div>
<a href="http://www.euro.who.int/en/health-topics/disease-prevention/nutrition/a-healthy-lifestyle/body-mass-index-bmi">Zoek meer informatie over BMI</a></div>


</body>
</html>