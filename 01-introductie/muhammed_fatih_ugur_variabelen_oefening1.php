<?php  

/*Variabelen - Variabelen */

$naam= "Muhammed Fatih";
$Naam= "Salih Yavuz";
$jaar= date("Y");
$car="Honda";
$color = "grey";

/*Variabelen - Variabelen - Uitbreiding*/
/*oppervlakte van rechthoek*/

$korte_zijde= 23;
$lange_zijde= 76;
$oppervlak_rechthoek= $korte_zijde*$lange_zijde;

/* bereken oppervlakte van rechthoek*/

$breedte= $_GET ['breedte']?? 23;
$hoogte= $_GET ['hoogte']?? 76;
$oppervlakte= $breedte*$hoogte;


?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Variabelen Oefening1</title>
    <style>

    body{
        width:1200px;
        text-align:left;
        padding-left:50px;
        background-color: #f2f2f2;
    }

    h1{
        font-size:1.3rem;
        color:darkblue;
    }
    form{
        width:380px;
        background-color:lightblue;
        padding-left:30px;
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 4px;
        padding:15px;
    }
    input[type=text] {
        width: 30%;
        padding: 6px 8px;
        margin: 4px 0;
        box-sizing: border-box;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
        }

    input[type=submit] {
        width:80%;
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 16px 32px;
        text-decoration: none;
        margin: 4px 2px;
        cursor: pointer;
        border-radius: 4px;
        }
    ul{
        background-color:darkblue;
        color:white;
        padding:10px;
        list-style:none;
        padding-left:15px;
        border-radius: 4px;
        }

    </style>

</head>  
<body>

<h1>Welkom de pagina van <?php echo $naam ?>...</h1>

<p>Mijn naam is <b><?php echo $naam ?></b>. Het is eerste oefening van variabelen <br> en ik moet mijn naam schrijven in een variabele.</p>
<p> Wij leven in  <?php echo $jaar ?> en volgend jaar is het: <?php echo date ("Y") +1?></p>
<p>Mijn naam is <b><?php echo $naam ?></b></p>   
<p>Mijn zoon is <b> <?php echo$Naam ?></b>. Hij is 5 jaar oud.</p>
<p>Ik heb een <b><?php echo $car ?></b>. De kleur van mijn auto is<b> <?php echo $color ?></b>.</p>

<h1>Variabelen | Variabelen | Uitbreiding</h1>

<p>De oppervlakte van rechthoek met de volgende afmetingen: 23m op 76m is:<br> <?php echo $korte_zijde ?> x <?php echo $lange_zijde ?> = <?php echo $oppervlak_rechthoek ?> m. </p>

<h1>Bereken oppervlakte | Uitbreiding</h1>

<p>Je kan ook oppervlakte van rechthoek berekenen met de formulier hier boeven...</p>


<form>
<p><b>Vul het formulier in om de oppervlakte te berekenen</b></p>
<p>Wat is de breedte: <input name="breedte" type="text" required>  (in cm of meter)</p>
<p>Wat is de hoogte: <input name="hoogte" type="text" required>  (in cm of meter)</p>
<p> <input type="submit" value="Berekenen!"></p>

<ul>

<li><p>Breedte: <?php echo $breedte ?></p></li>
<li><p>Hoogte: <?php echo $hoogte ?></p></li>
<p>De oppervlakte is:<b> <?php echo $oppervlakte ?><b>cm of m.</p>

</ul>
</form>

</body>
</html>
