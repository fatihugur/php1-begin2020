<?php  

//bmi.php?lengte=1.85&gewicht?78

$lengte=$_GET ['lengte']?? 1.85;
$gewicht=$_GET ['gewicht']?? 78;
$bmi = $gewicht /($lengte*$lengte);
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Oefeningen</title>

</head>  
<body>

<form>
<p><b>Bereken je eigen BMI</b></p>
<p>Lengte: <input name="lengte" type="text" required> (in meter)</p>
<p>Gewicht: <input name="gewicht" type="text" required>(in kg)</p>
<p> <input type="submit" value="Berekenen"></p>
</form>

<h1>BMI</h1>
<ul>
<li>lengte: <?php echo $lengte ?>m </li>
<li>Gewicht: <?php echo $gewicht ?>kg </li>
</ul>

<a href="bmi.php?lengte=1.50&gewicht=65"> Bekijk en toon </a>

<p>Het bmi is <b><?php echo round($bmi,2) ?><b>.</p>

</body>
</html>