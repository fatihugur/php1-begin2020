<?php  

/*opervlak van rechthoek*/

$korte_zijde= 23;
$lange_zijde= 76;
$opervlak_rechthoek= $korte_zijde*$lange_zijde;

/*bereken gemiddelde*/
$getal1= 5;
$getal2= 45;
$getal3= 87;
$getal4= 23;
$getal5= 57;
$getal6= 37;
$getal7= 18;
$bereken_gemiddelde= ($getal1+$getal2+$getal3+$getal4+$getal5+$getal6+$getal7)/7;


/* uitbreiding */
$cijfers= [5,45,87,23,57,37,18];
$gemiddelde= array_sum($cijfers)/7;



/*bereken leeftijd */
$geboortejaar= 1974;
$huidigejaar= date("Y");
$leeftijd= $huidigejaar-$geboortejaar;

/* bereken jouw BMI */

$lengte= 1.74;
$gewicht= 79;
$bmi = $gewicht /($lengte*$lengte);

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8">
    <title>Oefeningen opervlak van rechthoek |HTML&PHP_mengen</title>
    <style>
    h1{
        font-size:1.3rem;
        color:darkblue;
        padding-left:25px;
    }
    p{
        font-style: open sans;
        font-size:1.2rem;
        padding-left:30px;
    }

    </style>

</head>  
<body>

<h1> Wiskunde - Oefening 1 | Opervlakte berekenen</h1>

<p>De oppervlakte van rechthoek met de volgende afmetingen: 23m op 76m is <?php echo $opervlak_rechthoek ?> m. </p>

<h1> Wiskunde - Oefening 2 | Bereken het gemiddelde van getalen</h1>

<p>Het gemiddelde van de volgende getallen (5, 45, 87, 23, 57, 37, 18.) is: <?php echo $bereken_gemiddelde ?></p>

<p>Het gemiddelde van de volgende getallen (5, 45, 87, 23, 57, 37, 18.) is: <?php echo $gemiddelde ?></p>

<h1>Variabelen - Wiskunde - Uitbreiding 2 | Bereken jouw leeftijd</h1>

<p>Ik ben 1974 geboren en ik ben nu  <?php echo $leeftijd ?>  jaar oud</p>

<h1>Variabelen - Wiskunde - Uitbreiding 1 | Bereken jouw BMI op basis van jouw lengte en jouw gewicht</h1>

<p>Ik ben 1,74cm lengte en 79kg; mijn BMI is:  <?php echo $bmi ?>. </p>

</body>
</html>