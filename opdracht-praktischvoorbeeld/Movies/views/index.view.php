<!-- 

  Opsplitsen in include files
    - Sort form
    - MovieCard
    - Start HTML
    - End HTML
    - Footer (jaartal via PHP)
    - Aanpasbare Page title

  Array inlezen
  Array overlopen en elke film afdrukken met juiste gegevens
    - Synopsis afkorten op 100 tekens zodat het niet te lang wordt...
    - Aantal sterren tonen op (max 5 sterren) -> berekenen en afronden
    - link naar movie.php met ID in de url -> movie.php?id=3
      - op de volgende pagina kan je dat nummer gebruiken om de gekozen film uit te lezen

  BONUS: Array Sorteren via formulier
    $sort = $_GET['sort'];
    -> http://shiflett.org/blog/2011/sorting-multi-dimensional-arrays-in-php
    -> https://www.w3schools.com/php/func_array_multisort.asp

 -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <title>Overzicht - Marvel Movies</title>
</head>
<body>

  <div class="page-content">
    <div class="container">
      <h1><img src="images/marvel-logo.png" alt="Marvel Studios"></h1>

      <div class="movie-sort">
        <form class="sort-form" method="get">
          <select name="sort">
            <option value="0">-- Sort movies by</option>
            <option value="score">Score</option>
            <option value="title">Title</option>
            <option value="release">Release Date</option>
            <option value="director">Director</option>
          </select>
          <input type="submit" value="Sort!">
        </form>
      </div>

      <section class="movies">

        <?php foreach($movies as $movie): ?>

        <a href="movie.php?id=<?php echo $movie['id']?>" class="movieCard-link" name="<?php echo $movie['title'] ?>">
          <article class="movieCard">
            <div class="movieCard-image">
              <img src="<?php echo $movie['poster']?>" alt="<?php echo $movie['title']?>">
            </div>
            <div class="movieCard-content">
              <h1 class="movieCard-title"><?php echo $movie['title'] ?></h1>
              <p class="movieCard-release"><?php echo $movie['release']->format ("d F Y")?></p>
              
              <p class="movieCard-review">
              <?php 
                if (strlen($movie['synopsis'])>100):
                    echo substr($movie['synopsis'],0,100)."...";
                else:
                    echo $movie['synopsis'];
                endif;
              ?>
              </p>

              <p class="movieCard-director"><b>Directed By</b>: <br> 
              <?php echo $movie['director']?></p>
            </div>
            <div class="movieCard-score">
            <?php echo str_repeat("<i class='fas fa-star'></i>", round($movie['score'] / 2)) ?>

            </div>  
            <div class="movieCard-reviewCount">
                <i class="far fa-comment"></i> 
                <?php 
                $result= count($movie['reviews']);
                echo $result;
                ?>
            </div>
          </article>
        </a>
      <?php endforeach ?>
   
      </section>
    </div>
  </div>

  <footer class="page-footer">
    <div class="container">
      Marvel Studios - &copy; 2020
    </div>
  </footer>
  
</body>
</html>