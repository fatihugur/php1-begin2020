<!-- 

  Opsplitsen in include files
    - Start HTML
    - End HTML
    - Review
    - Footer

  Array inlezen en juiste film opzoeken  
    -> $id = $_GET['id'];
    -> Juiste velden afdrukken in de tekst
    -> Reviews overlopen
    -> Toon een melding "<p>Er zijn nog geen reviews</p>" als er geen reviews zijn.
        -> bv. iron man 3

  BONUS: Related movies opzoeken uit array en overlopen
    -> hergebruik de MovieCard include file
    -> Toon de volledige Related movies sectie ENKEL als er gerelateerde films zijn

ARRAY İÇİNDE ARRAY CAGIRMA/ met Bert

foreach ($movie['related']as $review)
    <article>
        echo$review
    </article>
endforeach


 -->
 <!DOCTYPE html>
 <html lang="en">
 <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
   <title>Iron Man - Marvel Movies</title>
 </head>
 <body>
 
   <div class="page-content">
     <div class="container">
       <h1><img src="images/marvel-logo.png" alt="Marvel Studios"></h1>
       <p class="text-center"><a href="index.php" class="back-btn">Back to list</a></p>
      
       <article class="movieDetail">
          <div class="movieDetail-image">
            <img src="<?php echo $movie['poster']?>" alt="<?php echo $movie['title']?>">
          </div>
          <div class="movieDetail-content">
            <header class="movieDetail-header">
                <div class="movieDetail-score">
                   <?php echo str_repeat("<i class='fas fa-star'></i>", round($movie['score'] / 2)) ?>
                </div>  
                <h1 class="movieDetail-title"><?php echo $movie['title'] ?></h1>
                <p class="movieDetail-release"><b>Released on</b>: 
                    <?php echo $movie['release'] -> format("d F Y")?></p>
                <p class="movieDetail-director"><b>Directed by</b>: <?php echo $movie['director']?></p>
            </header>

            <div class="movieDetail-synopsis">
              <p><?php echo $movie['synopsis']?></p>
            </div>
            <aside class="movieDetail-reviews">
              <h1 class="movieDetail-reviews-title">Reviews</h1>

              <?php if ($movie['reviews']== true):?>
              <?php foreach ($movie['reviews'] as $reviews):?>
              <article class="movieReview"> 
                    <?php echo $reviews ?>
              </article>
              <?php endforeach?>
              <?php else:?>
                <p><b>Er zijn geen reviews</b></p>
              <?php endif?>
            </aside>
          </div>
       </article>

       <section class="relatedMovies">
         <h1 class="relatedMovies-title">Related movies</h1>

         <section class="movies">
         
            <?php foreach ($movie['related'] as $related_movies):?>
    
            <a href="movie.php?id=<?php echo $movie ?>" class="movieCard-link">
              <article class="movieCard">
                <div class="movieCard-image">
                  <img src="<?php echo $movie['poster']?>" alt="<?php echo $movie['title']?>">
                </div>
                <div class="movieCard-content">
                  <h1 class="movieCard-title"><?php echo $movie ?></h1>
                  <p class="movieCard-release"><?php echo $movie['release'] -> format("d F Y")?></p>
                  <p class="movieCard-review">
                      <?php 
                        if (strlen($movie['synopsis'])>100):
                            echo substr($movie['synopsis'],0,100)."...";
                        else:
                            echo $movie['synopsis'];
                        endif;
                        ?>
                    </p>
                  <p class="movieCard-director"><b>Directed By</b>: <br> <?php echo $movie['director']?></p>
                </div>
                <div class="movieCard-score">
                    <?php echo str_repeat("<i class='fas fa-star'></i>", round($movie['score'] / 2)) ?>
                </div>  
                <div class="movieCard-reviewCount">
                <?php 
                $result= count($movie['reviews']);
                echo $result;
                ?>
                </div>
              </article>
            </a>
            <?php endforeach ?>
    
            <!-- 
                <a href="movie.php?id=3" class="movieCard-link">
                <article class="movieCard">
                  <div class="movieCard-image">
                    <img src="images/ironman3.jpg" alt="Iron Man 3">
                  </div>
                  <div class="movieCard-content">
                    <h1 class="movieCard-title">Iron Man 3</h1>
                    <p class="movieCard-release">23 april 2008</p>
                    <p class="movieCard-review">As depicted in the Marvel Cinematic Universe, Thor has long had some serious family issues -- but they truly all come to a head in Taika Wai...</p>
                    <p class="movieCard-director"><b>Directed By</b>: <br> Jon Favreau</p>
                  </div>
                  <div class="movieCard-score">
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                  </div>  
                  <div class="movieCard-reviewCount">
                      <i class="far fa-comment"></i> 0
                  </div>
                </article>
              </a>
              -->
          </section>
       </section>
     </div>
   </div>
 
   <footer class="page-footer">
     <div class="container">
       Marvel Studios - &copy; 2020
     </div>
   </footer>
   
 </body>
 </html>