<!-- 

- Maak een view file voor deze pagina.
- Gebruik de include files van de index pagina.
- Zorg ervoor dat het "aanbod" uit de "aanbod-array" komt.
- Als er geen items in de aanbod array zitten moet er een berichtje getoond worden.
	-> "Er is nog geen aanbod toegevoegd."

 -->
 <?php include "includes/start.php" ?>

 <?php include "includes/navigatie.php" ?>

    <div class="banner" style="background-image:url('images/brandon-morgan-19496-unsplash.jpg')">
      <div class="banner-content">
        <p>Onze aanbod!</p>
      </div>
    </div>

    <main class="content">
      <div class="container">
        <div class="text-center section">
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium accusamus eius unde, expedita ullam quisquam quibusdam, eum sequi illo excepturi a distinctio exercitationem rem voluptatem laboriosam quaerat. Reiciendis, incidunt sint.
          </p>
        </div>
      </div>
      <section class="section aanbod-container">
        <div class="container">
    
          <?php foreach ($aanbod as $title => $content): ?>
          <section class="aanbod">
            <h1><?php echo $title ?></h1>
            <p><?php echo $content['tagline'] ?></p>
            <p class="aanbod-price">€5 <?php echo $content['price'] ?>p.p.</p>
            <ul class="aanbod-featurelist">
        
            <?php if (count($content['features'])): ?>
            <?php foreach ($content['features'] as $feature): ?>
                <li><?php echo $feature ?></li>
            <?php endforeach ?>
            <?php else: ?>  
            <p>Er is nog geen aanbod toegevoegd.</p>
            <?php endif; ?>

           </ul>
          </section>

          <?php endforeach ?>
        </div>
        <div class="text-center">
          <a href="offerte.php" class="cta-button">Bereken hier uw prijs!</a>
        </div>
      </section>
      <?php include "includes/footer.php" ?>