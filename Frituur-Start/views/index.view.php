<!-- 

Maak een view file van deze pagina

 -->

<!-- "include" de start-html op elke pagina -->

<?php include "includes/start.php" ?>

		<!-- "include" de navigatie op elke pagina -->

        <?php include "includes/navigatie.php" ?>
        
		<!-- -------- -->

    <div class="banner" style="background-image:url('images/robin-stickel-82145-unsplash.jpg')">
      <div class="banner-content">
        <p>De lekkerste frietjes!</p>
      </div>
    </div>

    <main class="content">
      <div class="container">
<!-- -------- -->

        <div class="text-center section">
          <h1>Lorem ipsum dolor sit.</h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium accusamus eius unde, expedita ullam quisquam quibusdam, eum sequi illo excepturi a distinctio exercitationem rem voluptatem laboriosam quaerat. Reiciendis, incidunt sint.
          </p>
        </div>
        <div class="text-2-col section">
          <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis expedita alias nisi, nobis, illum exercitationem repellendus ipsum dolore corporis deserunt placeat, quasi ipsam deleniti iure. Quam id laboriosam suscipit debitis.</p>
          <p>Necessitatibus optio repellat eius placeat blanditiis veritatis, repudiandae dolorem natus. Voluptatibus, eaque nam! Pariatur assumenda magni, placeat animi amet odit, ut, maxime vel molestiae blanditiis voluptate iure quisquam enim rerum!</p>
          <p>Odit, eveniet perspiciatis explicabo corporis laboriosam tenetur enim natus placeat ut voluptatum neque culpa nulla quae consequuntur repellat consectetur beatae. Ipsum magni natus dolorum ea culpa vel, eligendi quibusdam aspernatur?</p>
          <p>Possimus architecto explicabo, aut, vero voluptate eaque illo fugiat dolorem cum, cupiditate sint blanditiis at laboriosam. Ipsam culpa nesciunt doloribus eius, quam, dolor quis quia alias possimus ipsa unde perspiciatis.</p>
        </div>
      </div>


<!-- "include" de end-html en "page-bottom" op elke pagina -->
   <?php include "includes/footer.php" ?>
<!-- -------- -->