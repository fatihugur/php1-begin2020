<!-- 

- Maak een view file voor deze pagina.
- Gebruik de include files van de index pagina.


- Het formulier gebruikt de POST methode. Hiervoor kunnen we gebruik maken van $_POST[] in PHP om de gegevens op te vragen.
	Deze komen niet in de URL van de pagina.
	
- Maak een lege foutmeldingen array
- Als het formulier verzonden is moet je enkele controles doen op de formulier data
	- Gebruik de "empty()" en "isset()" functie om te controleren of de gegevens ingevuld zijn en of het vinkje aangevinkt is.
	- if (empty($_POST['naam])) { $foutmeldingen['naam'] = 'Naam is niet ingevuld'; }
	- toon de foutmeldingen in de view in een rode tekst kleur bij de formulier elemeenten.
	  in de view: if(isset($foutmeldingen['naam'])) ... echo $foutmeldingen['naam']; ...
	- Als de foutmeldingen array leeg is kan je een "mail" versturen. Gebruik hiervoor de "mail()" functie van PHP.

- Toon in de HTML een success berichtje als het formulier verzonden is en de foutmeldingen array leeg is.

 -->
 <?php include "includes/start.php" ?>

 <?php include "includes/navigatie.php" ?>

    <div class="banner" style="background-image:url('images/elli-o-43178-unsplash.jpg')">
      <div class="banner-content">
        <p>Contacteer ons!</p>
      </div>
    </div>

    <main class="content">
      <div class="container">
        <div class="text-center section">
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium accusamus eius unde, expedita ullam quisquam quibusdam, eum sequi illo excepturi a distinctio exercitationem rem voluptatem laboriosam quaerat. Reiciendis, incidunt sint.
          </p>
        </div>

        <div class="message">
          Het bericht is verstuurd!
        </div>

        <form class="contact-form" method="post">
          <p>
            <label for="naam">Naam</label>
            <input type="text" id="naam">
          </p>
          <p>
            <label for="email">Email</label>
            <input type="text" id="email">
          </p>
          <p>
            <label for="bericht">Bericht</label>
            <textarea id="bericht"></textarea>
          </p>
          <p>
            <label>
              <input type="checkbox"> 
              Ja, ik heb de privacy policy gelezen en ik ga akkoord met de voorwaarden.
            </label>
          </p>
          <p>
            <input type="submit" value="Verstuur bericht!">
          </p>
        </form>
      </div>
   
      <?php include "includes/footer.php" ?>