<!-- 

- Maak een view file voor deze pagina.
- Gebruik de include files van de index pagina.


- Zorg ervoor dat de prijsberekening enkel getoond wordt als het formulier verzonden is
- Zorg ervoor dat de correcte prijs berekend wordt voor de verzonden items

- Zorg ervoor dat het formulier enkel getoond wordt als het nog niet verstuurd is.
- Zorg ervoor dat het "aanbod" in het formulier uit de "aanbod-array" komt.

 -->
 <?php include "includes/start.php" ?>

 <?php include "includes/navigatie.php" ?>

    <div class="banner" style="background-image:url('images/brandon-morgan-105310-unsplash.jpg')">
      <div class="banner-content">
        <p>Bereken jouw prijs!</p>
      </div>
    </div>

    <main class="content">
      <div class="container">
        <div class="text-center section">
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium accusamus eius unde, expedita ullam quisquam quibusdam, eum sequi illo excepturi a distinctio exercitationem rem voluptatem laboriosam quaerat. Reiciendis, incidunt sint.
          </p>
        </div>

				<!-- Uitkomst enkel tonen als formulier verzonden is -->
        <div class="offerte-uitkomst text-center">
          <p>Voor 20 personen</p>
          <h2>€320</h2>
          <p>Inclusief vaste kost: 100eur</p>

          <a class="cta-button" href="offerte.php">Bereken opnieuw!</a>
        </div>

				<!-- Formulier enkel tonen als het nog niet verstuurd is. -->
        <form class="offerte-form">
          <p>
            <label for="aantal"><b>Aantal personen</b>:</label>
            <input type="number" value="20">
          </p>
          <p><b>Kies je formule:</b></p>

          <div class="formules">
            <input name="formule" type="radio" value="formule1" id="formule1">
            
          <?php foreach ($aanbod as $title => $content): ?>
            <label for="formule1" class="formule">
              <h2><?php echo $title ?></h2>
                <p><?php echo $content['tagline'] ?></p>
                <p>€<?php echo $content['price'] ?> p.p.</p>
            </label>
          <?php endforeach ?>

          </div>
          <p class="text-center">
            <input type="submit" value="Berekenen!">
          </p>
        </form>
      </div>

      <?php include "includes/footer.php" ?>