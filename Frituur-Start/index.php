<!-- 

Maak een view file van deze pagina

 -->

<!-- "include" de start-html op elke pagina -->
<?php include "includes/start.php" ?>

		<!-- "include" de navigatie op elke pagina -->
<?php include "includes/navigatie.php" ?>
		<!-- -------- -->

    <div class="banner" style="background-image:url('images/robin-stickel-82145-unsplash.jpg')">
      <div class="banner-content">
        <p>De lekkerste frietjes!</p>
      </div>
    </div>

    <main class="content">
      <div class="container">
<!-- -------- -->

        <div class="text-center section">
          <h1>Lorem ipsum dolor sit.</h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium accusamus eius unde, expedita ullam quisquam quibusdam, eum sequi illo excepturi a distinctio exercitationem rem voluptatem laboriosam quaerat. Reiciendis, incidunt sint.
          </p>
        </div>
        <div class="text-2-col section">
          <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis expedita alias nisi, nobis, illum exercitationem repellendus ipsum dolore corporis deserunt placeat, quasi ipsam deleniti iure. Quam id laboriosam suscipit debitis.</p>
          <p>Necessitatibus optio repellat eius placeat blanditiis veritatis, repudiandae dolorem natus. Voluptatibus, eaque nam! Pariatur assumenda magni, placeat animi amet odit, ut, maxime vel molestiae blanditiis voluptate iure quisquam enim rerum!</p>
          <p>Odit, eveniet perspiciatis explicabo corporis laboriosam tenetur enim natus placeat ut voluptatum neque culpa nulla quae consequuntur repellat consectetur beatae. Ipsum magni natus dolorum ea culpa vel, eligendi quibusdam aspernatur?</p>
          <p>Possimus architecto explicabo, aut, vero voluptate eaque illo fugiat dolorem cum, cupiditate sint blanditiis at laboriosam. Ipsam culpa nesciunt doloribus eius, quam, dolor quis quia alias possimus ipsa unde perspiciatis.</p>
        </div>
      </div>


<!-- "include" de end-html en "page-bottom" op elke pagina -->
    </main>
	</div>
  <div class="page-bottom">
    <footer class="main-footer">
      <div class="container">
        <div class="footer-columns">
          <aside class="sitemap">
            <h1>Site</h1>
            <ul>
              <li> <a href="index.php">Frituur</a> </li>
              <li> <a href="aanbod.php">Aanbod</a> </li>
              <li> <a href="offerte.php">Offerte</a> </li>
              <li> <a href="contact.php">Contact</a> </li>
            </ul>
          </aside>
          <aside>
            <h1>Contact</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem cumque a tempora dolorem tempore perspiciatis debitis labore quia saepe odit adipisci necessitatibus deleniti est sint nam repellat similique, culpa eaque.</p>
          </aside>
          <aside>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2508.7170280983582!2d5.326677552093597!3d51.039846879460704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c12594d5049df9%3A0xe4e217ce1e2c8c54!2sSchachtplein+1%2C+3550+Heusden-Zolder!5e0!3m2!1snl!2sbe!4v1526967725698" frameborder="0" style="border:0" allowfullscreen></iframe>
          </aside>
        </div>
      </div>
    </footer>
  </div>
</body>
</html>
<!-- -------- -->